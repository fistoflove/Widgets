<?php
namespace Annex\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class ImageMozaic extends Template implements BlockInterface
{
    protected $_template = "widget/image-mozaic.phtml";

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Annex\Helper\Block\Rewrite\ProductHelper $annexProductHelper,
        array $data = []
    ) {
        $this->annexProductHelper = $annexProductHelper;
        parent::__construct($context, $data);
    }

    public function getProductData($_product_id)
    {
        return $this->annexProductHelper->getTheData($_product_id);
    }
}