<?php
namespace Annex\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class ReviewShowcase extends Template implements BlockInterface
{
    protected $_template = "widget/review-showcase.phtml";
}