<?php
namespace Annex\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class CategoryImages extends Template implements BlockInterface
{
    protected $_template = "widget/category-images.phtml";
}