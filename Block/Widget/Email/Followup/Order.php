<?php
namespace Annex\Widgets\Block\Widget\Email\Followup;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;


class Order extends Template implements BlockInterface
{
    protected $_template = "widget/email/followup/order-test.phtml";

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection,
        \Annex\Helper\Helper\Product $annexProductHelper,
        \Annex\Helper\Helper\Collection $annexCollectionHelper,
        array $data = []
    ) {
        $this->customerRepository = $customerRepository;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderCollection = $orderCollection;
        $this->annexProductHelper = $annexProductHelper;
        $this->annexCollectionHelper = $annexCollectionHelper;
        parent::__construct($context, $data);
    }

    public function getCustomer($email)
    {
        try {
            $customer = $this->customerRepository->get($email, 1);
        } catch (NoSuchEntityException $e) {
            return false;
            throw new \Magento\Framework\Exception\LocalizedException(__("The customer email isn't defined."));
        }
        return $customer;
    }

    public function getCategories($product_id)
    {
        echo "product_id: " . $product_id;
        $product = $this->annexProductHelper->getTheProduct($product_id);
        $categories = $this->annexProductHelper->getProductCategories($product);
        return $categories;
    }

    public function getLastestOrder($customer_email)
    {
        $orders = $this->orderCollectionFactory->create()
        ->addAttributeToFilter('customer_email', $customer_email)
        ->addAttributeToFilter('status', ['complete', 'exported'])
        ->setOrder('created_at','desc')
        ->load();

        if($orders->count() > 0) {
            return $orders->getFirstItem();
        } else {
            return false;
        }

        return $data;
    }

    public function getLastestOrderItems($last_order)
    {
        $orderItems = $last_order->getItems();
        return $orderItems;

    }

    public function getLastestOrderCategories($products)
    {
        $categories = [];

        if($products) {
            foreach($products as $product) {
                $pro =  $this->annexProductHelper->getTheProduct($product->getProductId());
                $cat_ids = $pro->getCategoryIds();
                foreach($cat_ids as $cat_id) {
                    array_push($categories, $cat_id);
                }
            }
            return array_values(array_unique($categories,SORT_NUMERIC));
        }
    }

    public function getTemplateData($categories)
    {
        $data = [
            "blocks" => [
                322 => [
                    'name' => 'Featured Products',
                    'id' => 10,
                    'landing' => 'https://baggotstreetwines.com/',
                    'block_title' => 'Featured Products Block',
                ],
                309 => [
                    'name' => 'Staff Favourites',
                    'id' => 10,
                    'landing' => 'https://baggotstreetwines.com/staff-favourites/all.html',
                    'block_title' => 'Staff Favourites Block',
                ]
            ]
        ];
        $count = 4;
        $params = [
            3 => [
                'name' => 'Sample Category 3',
                'id' => 3,
                'landing' => 'https://baggotstreetwines.com/wine.html',
                'block_title' => 'Sample Upsell Block',
            ],
            24 => [
                'name' => 'Sample Category 24',
                'id' => 24,
                'landing' => 'https://baggotstreetwines.com/wine.html',
                'block_title' => 'Sample Upsell Block',
            ],
            31 => [
                'name' => 'Sample Category 31',
                'id' => 31,
                'landing' => 'https://baggotstreetwines.com/wine.html',
                'block_title' => 'Sample Upsell Block',
            ],
            114 => [
                'name' => 'Wine',
                'id' => 114,
                'landing' => 'https://baggotstreetwines.com/wine.html',
                'block_title' => 'Wine Upsell Block',
            ],
            115 => [
                'name' => 'Whiskey',
                'id' => 115,
                'landing' => 'https://baggotstreetwines.com/whiskey.html',
                'block_title' => 'Whiskey Upsell Block',
            ],
            116 => [
                'name' => 'Fine Wine',
                'id' => 116,
                'landing' => 'https://baggotstreetwines.com/fine-wine.html',
                'block_title' => 'Fine Wine Upsell Block',
            ],
            117 => [
                'name' => 'Sparkling Wine',
                'id' => 117,
                'landing' => 'https://baggotstreetwines.com/sparkling-wine.html',
                'block_title' => 'Sparkling Wine Upsell Block',
            ],
            118 => [
                'name' => 'Spirits',
                'id' => 118,
                'landing' => 'https://baggotstreetwines.com/spirits.html',
                'block_title' => 'Spirits Upsell Block',
            ],
            119 => [
                'name' => 'Craft Beer',
                'id' => 119,
                'landing' => 'https://baggotstreetwines.com/craft-beer.html',
                'block_title' => 'Craft Beer Upsell Block',
            ]
            ];

        foreach($categories as $cat) {

            if(array_key_exists($cat, $params)) {
                array_unshift($data['blocks'], $params[$cat]);
            }
        }

        foreach($data['blocks'] as $k => $v) {
            $products = $this->annexCollectionHelper->getCollection($v['id']);
            $data['blocks'][$k]['products'] = [];

            foreach($products as $product) {
                $_product = $this->annexProductHelper->getTheProduct($product->getId());
                $_product_data = $this->annexProductHelper->getTheData($_product);
                array_push($data['blocks'][$k]['products'], $_product_data);
            }
        }

        // echo json_encode($data);

        return $data;
    }
}
