<?php
namespace Annex\Widgets\Block\Widget\Email\Followup;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;


class Cart extends Template implements BlockInterface
{
    protected $_template = "widget/email/followup/order-test.phtml";

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Annex\Helper\Helper\Product $annexProductHelper,
        \Annex\Helper\Helper\Collection $annexCollectionHelper,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        array $data = []
    ) {
        $this->customerRepository = $customerRepository;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->annexProductHelper = $annexProductHelper;
        $this->annexCollectionHelper = $annexCollectionHelper;
        $this->quoteFactory = $quoteFactory;
        parent::__construct($context, $data);
    }

    public function getQuoteProducts($id)
    {
        $quote = $this->quoteFactory->create()->load($id);
        $products = [];
        foreach ($quote->getAllItems() as $item) {
            $product = $this->annexProductHelper->getTheProduct($item->getItemId());
            array_push($products, $this->annexProductHelper->getTheData($product));
            // array_push($products, $this->annexProductHelper->getTheData($quote->getItemById($item->getItemId())));
        }
        return $products;
    }

    public function getCustomer($email)
    {
        try {
            $customer = $this->customerRepository->get($email, 1);
        } catch (NoSuchEntityException $e) {
            return false;
            throw new \Magento\Framework\Exception\LocalizedException(__("The customer email isn't defined."));
        }
        return $customer->getId();
    }

    public function getCategories($product_id)
    {
        echo "product_id: " . $product_id;
        $product = $this->annexProductHelper->getTheProduct($product_id);
        $categories = $this->annexProductHelper->getProductCategories($product);
        return $categories;
    }

    public function getLastestOrder($customerId)
    {
        $orders = $this->orderCollectionFactory->create($customerId);
        //Add all field to Collection
        $orders->addFieldToSelect('*');
        // Collection filter by Customer Id
        // sort order by  create date descending
        $orders->setOrder('created_at','desc');
        // limit collection to 1
        $orders->setPageSize(1)->setCurPage(1);
        if($orders->count() > 0){
            // echo ($orders->count());
            $lastestOrder = $orders->getFirstItem();

            return $lastestOrder;
        }else{
            // no order found
            $lastestOrder = false;
        }

    }

    public function getLastestOrderItems($last_order)
    {
        $orderItems = $last_order->getItems();
        return $orderItems;

    }

    public function getLastestOrderCategories($products)
    {
        $categories = [];

        if($products) {
            foreach($products as $product) {
                $pro =  $this->annexProductHelper->getTheProduct($product->getProductId());
                $cat_ids = $pro->getCategoryIds();
                foreach($cat_ids as $cat_id) {
                    array_push($categories, $cat_id);
                }
            }
            return array_values(array_unique($categories,SORT_NUMERIC));
        }
    }

    public function getTemplateData($categories)
    {
        $data = [
            "blocks" => [
                10 => [
                    'name' => 'Staff Favourites',
                    'id' => 10,
                    'landing' => 'https://baggotstreetwines.com/staff-favourites/all.html',
                    'block_title' => 'Staff Favourites Block',
                ]
            ]
        ];
        $count = 4;
        $params = [
            9 => [
                'name' => 'Sample Category',
                'id' => 9,
                'landing' => 'https://baggotstreetwines.com/wine.html',
                'block_title' => 'Sample Upsell Block',
            ],
            114 => [
                'name' => 'Wine',
                'id' => 114,
                'landing' => 'https://baggotstreetwines.com/wine.html',
                'block_title' => 'Wine Upsell Block',
            ],
            115 => [
                'name' => 'Whiskey',
                'id' => 115,
                'landing' => 'https://baggotstreetwines.com/whiskey.html',
                'block_title' => 'Whiskey Upsell Block',
            ],
            116 => [
                'name' => 'Fine Wine',
                'id' => 116,
                'landing' => 'https://baggotstreetwines.com/fine-wine.html',
                'block_title' => 'Fine Wine Upsell Block',
            ],
            117 => [
                'name' => 'Sparkling Wine',
                'id' => 117,
                'landing' => 'https://baggotstreetwines.com/sparkling-wine.html',
                'block_title' => 'Sparkling Wine Upsell Block',
            ],
            118 => [
                'name' => 'Spirits',
                'id' => 118,
                'landing' => 'https://baggotstreetwines.com/spirits.html',
                'block_title' => 'Spirits Upsell Block',
            ],
            119 => [
                'name' => 'Craft Beer',
                'id' => 119,
                'landing' => 'https://baggotstreetwines.com/craft-beer.html',
                'block_title' => 'Craft Beer Upsell Block',
            ]
            ];

        foreach($categories as $cat) {

            if(array_key_exists($cat, $params)) {
                array_unshift($data['blocks'], $params[$cat]);
            }
        }

        foreach($data['blocks'] as $k => $v) {
            $products = $this->annexCollectionHelper->getCollection($v['id']);
            $data['blocks'][$k]['products'] = [];

            foreach($products as $product) {
                $_product = $this->annexProductHelper->getTheProduct($product->getId());
                $_product_data = $this->annexProductHelper->getTheData($_product);
                array_push($data['blocks'][$k]['products'], $_product_data);
            }
        }

        // echo json_encode($data);

        return $data;
    }
}




