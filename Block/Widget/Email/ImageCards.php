<?php
namespace Annex\Widgets\Block\Widget\Email;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class ImageCards extends Template implements BlockInterface
{
    protected $_template = "widget/email/image-cards.phtml";
}