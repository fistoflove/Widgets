<?php
namespace Annex\Widgets\Block\Widget\Email;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class BgColor extends Template implements BlockInterface
{
    protected $_template = "widget/email/bg-color.phtml";
}