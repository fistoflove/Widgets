<?php
namespace Annex\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Cms80 extends Template implements BlockInterface
{
    protected $_template = "widget/cms-80.phtml";
}