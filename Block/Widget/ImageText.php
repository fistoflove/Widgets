<?php
namespace Annex\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class ImageText extends Template implements BlockInterface
{
    protected $_template = "widget/image-text.phtml";
}