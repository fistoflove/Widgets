<?php
namespace Annex\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class IconBlock extends Template implements BlockInterface
{
    protected $_template = "widget/icon-block.phtml";
}