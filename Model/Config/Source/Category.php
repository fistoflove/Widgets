<?php
namespace Annex\Widgets\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;


class Type implements OptionSourceInterface
{

    public function __construct( 
        \Magento\Catalog\Helper\Category $categoryHelper     
    ) {
        $this->_categoryHelper = $categoryHelper;
    }

    /**
     * Retrieve current store level 2 category
     *
     * @param bool|string $sorted (if true display collection sorted as name otherwise sorted as based on id asc)
     * @param bool $asCollection (if true display all category otherwise display second level category menu visible category for current store)
     * @param bool $toLoad
     */

    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
    {
        return $this->_categoryHelper->getStoreCategories($sorted , $asCollection, $toLoad);
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $categories = $this->getStoreCategories(true, true, true);
        $result = [];
        foreach($categories as $cat) {
            array_push($result,[
                'value' => $cat->getId(),
                'label' => $cat->getName()
            ]);
        };
        return $result;
    }
}
